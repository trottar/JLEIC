From holt@aspen.phy.anl.gov Wed Sep 20 12:00:57 2000
Date: Wed, 20 Sep 2000 11:56:29 -0500 (CDT)
From: Roy Holt <holt@aspen.phy.anl.gov>
To: Paul Reimer <reimer@aspen.phy.anl.gov>
Subject: Your report (fwd)



Roy J. Holt
Physics Division 
Argonne National Laboratory
Argonne, Illinois  60439-4843
e-mail: holt@anl.gov
phone:  630-252-4101
fax  :  630-252-3903

---------- Forwarded message ----------
Date: Tue, 19 Sep 2000 17:27:28 -0500
From: Craig Roberts <roberts@theory.phy.anl.gov>
Reply-To: cdroberts@anl.gov
To: holt@aspen.phy.anl.gov
Subject: Your report

Hello Roy,
      
      I've read your contribution and have appended some comments, for
what they're worth.  They cover theory because: a) you understand the
experimental limitations better than I do; and b) I think I understand
what you wrote about the experimental possibilities anyway, so those
sections must be fine.

Cheers, Craig.
      
1) Par. 1.  You write "...Schwinger-Dyson Equation (DSE) model..."
For consistency with the acronym, Dyson-Schwinger would be better.

2) Par. 1.  I might write "contemporary" instead of "state-of-the-art"
although the calculations are the best we can do.

3) Par. 1.  The pions and kaons ARE the Goldstone modes of QCD, not
just of chiral models.  That has been rigorously proven: the pion is
that massless bound state of a dressed-quark and anti-quark that
arises because of dynamical chiral symmetry breaking.

4) Par. 1.  Dynamically broken chiral symmetry is responsible for the
fact that the pion is much lighter than the rho.  In this sense the
light meson spectrum exhibits the consequences of dynamically broken
chiral symmetry.

5) Par. 2.  I don't understand the comment that sea-quarks arise from
higher-order effects in the nucleon while they are leading order in
the pion.  I expect and wrote that a similar mechanism is required in
both cases: a meson loop correction.  (See last sentence of para above
(27) in Hecht et al.)

New Section: PREVIOUS...

6) Par. 1.  I anticipate that a reader will be confused by comparing
the statement "Hecht et al. have shown ..." with the figure, where our
curve does not agree with the data.  The simplest ruse would be to
modify the figure caption to say: "...represents a dynamical
calculation..."

New Section: SIMULATION...

7) Par. 2.  "accpetance" -> acceptance

