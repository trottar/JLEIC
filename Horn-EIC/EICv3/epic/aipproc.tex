
% Start of file aipproc.tex. Documentation file for AIP Press aipproc style.
% Version number 1a, 10/17/95.
% Copyright (C), 1993, American Institute of Physics.

\documentstyle{article}

\if@twoside
\oddsidemargin  -.4in
\evensidemargin -.1in
\marginparwidth 107pt
\else
\oddsidemargin  -.25in
\evensidemargin -.25in
\marginparwidth 30pt
\fi
\marginparsep 6pt
 
\topmargin -61pt
 
\headheight 25pt
\headsep 16pt
 
\topskip 10pt
 
\footskip 30pt
\textheight = 55\baselineskip
\advance\textheight by \topskip



\textwidth42.5pc
\columnsep 1.5pc

%\def\thepart          {\Roman{part}} %
\def\thesection       {\arabic{section}.}
%\def\p@section        {}
\def\thesubsection    {\thesection\arabic{subsection}}
%\def\p@subsection     {\thesection\,}
%\def\thesubsubsection {\arabic{subsubsection}.}
%\def\p@subsubsection  {\thesection\,\thesubsection\,}
%\def\theparagraph     {\alph{paragraph}}
%\def\thesubparagraph  {\theparagraph.\arabic{subparagraph}}
%
\makeatletter
\def\@oddhead{\hfill\verb+aipproc.tex+}
\let\@evenhead\@oddhead
%\newenvironment{boxit}%
%   {\begin{center} \begin{tabular}{|@{\hspace{.15in}}c@{\hspace{.15in}}|}
%                   \hline \\ \begin{minipage}[b]{3.1in}}
%%                   \hline \\ \begin{minipage}[t]{3.1in}}
%   {\end{minipage} \\ \\ \hline \end{tabular} \end{center}}
\def\se{\vskip3pt plus1pt minus1pt\setbox0=\hbox to\hsize\bgroup\hss
        \vrule width.5pt
        \vbox\bgroup \hrule width \hsize height.5pt
        \vskip3pt\hbox to\hsize\bgroup\hss\vbox\bgroup\advance\hsize by-9pt
        \columnwidth\hsize\small}
\def\ee{\par\egroup\hss\egroup\vskip3pt\hrule width\hsize height.5pt\egroup
        \vrule width.5pt\hss\egroup
        \box0 \vskip3pt plus1pt minus1pt}
\def\latexe{\LaTeX\kern.15em 2${}_{\textstyle\varepsilon}$}

\flushbottom

%%%%

\makeatother

\flushbottom
\begin{document}
\sloppy		

\begin{titlepage}
\vspace*{0pt plus1fill}
\begin{center}
\LARGE\bf
AIP Press Toolbox for Camera-Ready Proceedings:\\[1pc]
User Guide for Proceedings
\end{center}
\vspace*{0pt plus1fill}
\vspace*{0pt plus1fill}
\hbox to\hsize{\hfil
\vbox{\offinterlineskip\hrule height 2pt\vskip4pt
\Large\sf\setbox0=\hbox{PRESS}\hbox to\wd0{\hfil AIP\hfil}
\vskip4pt
\box0
\vskip4pt \hrule height 2pt}%
\hfil}
\end{titlepage}

\newpage

\bgroup
\large \parindent0pt \advance\leftskip by1in \advance\rightskip by1in

This toolbox is the third in a series of Publishing Toolboxes developed
by the American Institute of Physics, independently or in collaboration with
member societies.

\vspace{1pc}

It is provided for the use of AIP authors for its publications. Any other
use is prohibited without express written permission from AIP.

\vspace{1pc}

Copyright (C) 1993, American Institute of Physics.


\vspace{0pt plus 1fill}

AIP Publishing toolboxes:

\vspace{1pc}

{\advance\leftskip by2pc

\hangindent2pc
WordPerfect Manuscript Toolbox (M1)\\
{\it (developed in collaboration with the American Vacuum Society)}

\hangindent2pc
REV\TeX\ Manuscript Toolbox (M2)\\
{\it (developed in collaboration with the American Physical Society and
the Optical Society of America)}

\hangindent2pc
AIP Press Toolbox (\verb+aipproc+)\\

}

\addtocounter{page}{-1}
\thispagestyle{empty}


\newpage

\begin{center}
\LARGE\bf
Preface
\vspace{2pc}
\end{center}

The roles of the author, compositor, and publisher continue to evolve as
technology advances. These changes are in part the result of the increasing
availability of typesetting technology to the author community.

\vspace{1pc}

The American Institute of Physics has responded by implementing composition
technology that will interface well with author tools. Where no tools are
available to meet the essential needs of the author and AIP, we will respond
by developing one. The toolbox supplied here is the result of one such
development effort.


\egroup

\newpage

\def\contentsname{\LARGE\bf Contents}

{\large
\tableofcontents
}


\twocolumn

\section{General Information}

This toolbox is made available to authors who are preparing their books
in camera-ready format. The author is expected to know \LaTeX\ and have it
running. The author is expected to conform to style requirements and to
deliver high-quality pages as well as the \TeX\ file that created them.

In order for AIP Press to provide the fastest possible publication, each
author will type his/her manuscript and lay it out into pages with art and
tables in place. The proceedings will be produced by a photo-offset process
directly from the pages (camera-ready copy) supplied by the author.
Please check over your paper carefully and thoroughly proofread
it before submission. Remember, the way you submit your manuscript
to the publisher is the way you will see it in print.

To ensure that your paper is reproduced clearly and in consistent size
and format, please carefully follow the instructions given in this
document. All the instructions are important, so please follow them.

\section{Toolbox Contents}

\begin{verbatim}
   README                   aipproc.cls
   aipproc.tex              aipproc.sty
   samp0.tex                samp1.tex
   samp2.tex                sampmain.tex
\end{verbatim}

It's very important that users read \verb+README+ and
\verb+aipproc.tex+ carefully.
 

\section{Guidelines}

\subsection{Introduction}

{\tt aipproc} runs with both \LaTeX~2.09 and \latexe\ under {\tt compatibility 
mode}, namely, you should use 

\begin{verbatim}
\documentstyle[Options]{aipproc}
\end{verbatim}
\noindent {\it rather than }

\begin{verbatim}
\documentclass
\usepackage{Options}
\end{verbatim}


Support for \LaTeX~2.09 is, if any, very limited.

Familiarity with
\LaTeX, and a copy of {\it \LaTeX: A Document Preparation System}
(second edition, by Leslie Lamport) 
and {\it The \LaTeX~Companion} (by Goosens, Mittelbach, and Samarin)
are assumed. You must also have a complete 
\TeX, \LaTeX\ installation before using the \verb+aipproc+ style.
For those with \latexe{} installation, you should  obtain at least the 
\verb+graphics+ and \verb+tools+ bundles in order to have most of the
supported features.

When commands are discussed they will be typeset in the form
\verb+\command+
showing that no arguments are required, the form
\verb+\command{#1}+
showing that one argument is required, etc.
Optional arguments and ``star forms'' of commands may exist: reference the
\LaTeX\ documentation for complete information when in doubt.

\subsection{Top matter}

In general, the beginning of your paper will look like this:
%%%%%
\se\begin{verbatim}
\documentstyle{aipproc}
\begin{document}
\title{Insert title here}
\author{Insert author names here}
\address{Insert author addresses here}
\maketitle
\begin{abstract}
  . . . Insert abstract here . . .
\end{abstract}
\end{verbatim}
\begin{center}
$\vdots$ \par
body of the paper goes here \par
$\vdots$ \par
\end{center}
\begin{verbatim}
\end{document}
\end{verbatim}\ee\noindent
Text font size is set to 12 pt already by the \verb+aipproc+, therefore, no
type size option is needed.  You need to add all the packages to be used with
\verb+aipproc+ to \verb+\documentstyle+'s options list. See \verb+samp2.tex+
for example.


Footnotes can be added within the \verb+\author{#1}+ and \verb+\address{#1}+
commands via the \verb+\thanks{#1}+ command. The correspondence
between authors and addresses should be indicated with manually inserted
superscript symbols: $*$, $\dagger$, $\ddagger$, $\|$, $\P$, $\S$,
 $**$, $\dagger\dagger$, etc. See \verb+samp0.tex+ for examples.

\subsection{Section headings}

Section headings are input as in \LaTeX. The output is similar.

Four levels of headings are provided in the \verb+aipproc+ style:
\verb+\section{#1}+, \verb+\subsection{#1}+, \verb+\subsubsection{#1}+, and
\verb+\paragraph{#1}+.

All text in the \verb+\section{#1}+ command is automatically set uppercase.
If a lowercase ``x'' is needed, just use \verb+\lowercase{x}+. For
example, to have ``Au'' in a \verb+\section+ command, type
\verb+\section{A\lowercase{u}}+.
On the rare occasions when you have to put commands such as \verb+\cite,
\ref+ in a section title, you must use \verb+\lowercase+ to prevent the
reference or citation key from being turned into uppercase. For example,
\verb+\section*{Title \lowercase{\protect\cite{baz}}}+.


All section headings are either unnumbered or automatically numbered.
Numbered sections may be cross referenced easily. Following the
\verb+\section{#1}+ command ({\it not\/} inside \verb+#1+) insert a
\verb+\label{#1}+
command, where \verb+#1+ is some combination of letters, numbers, and
punctuation. If
you later use \verb+\ref{#1}+, where \verb+#1+ is identical to that of the
\verb+\label{#1}+
command, then the correct section number will be produced by the
\verb+\ref{#1}+ command.

Use \verb+\protect\\+ to force a line break in a section heading. (Fragile
commands must be protected in section headings and captions, and \verb+\\+
is a fragile command. See Sec.\ \ref{hints} for a way around this.)

{\bf Important note.}
Authors should use the ``star'' form of these
commands to suppress all the automatic section numbering; e.g.,
\se\begin{verbatim}
\section*{First-level Heading}}
\subsection*{Second-level Heading}}
\subsubsection*{Third-level Heading}}
\end{verbatim}\ee\noindent
as numbered sections will not normally be needed for proceedings.

\subsection{Footnotes}
You may set footnotes using the normal \LaTeX{} mechanisms. Simplest is to
just use the \verb+\thanks{#1}+ command when setting footnotes inside
the  \verb+\author{#1}+ or \verb+\address{#1}+ commands, and the
\verb+\footnote{#1}+ command when setting footnotes in the text.\footnote{That
is how this footnote was set!}

You should use \verb+\tablenote{#1}+ command to produce table's footnote.

\subsection{Displayed equations}
\subsubsection[Setting and numbering displayed equation]{Setting and numbering
displayed\protect\\ equations}


The most common type of displayed equation
is a narrow, single-line equation, with or without an equation
number on the same line.

To set an unnumbered, single-line equation use the \verb+\[ \]+  construct:
\se\begin{verbatim}
\[
  E=mc^2.
\]
\end{verbatim} \ee\noindent
will typeset as
\se\[
E=mc^2.  
\] \ee

To set an automatically numbered, single-line equation use the equation
environment:
\se\begin{verbatim}
\begin{equation}
  E=mc^2.
\end{equation}
\end{verbatim} \ee\noindent
will typeset as
\se\begin{equation}
E=mc^2.
\end{equation}\ee\noindent

Breaking the equation into multi-line format may be necessary for very long
equations or for purposes of exposition.
The eqnarray environment is used for this. In this
environment a \verb+\\+ signifies the start of a new line, and the lines
are aligned by the material (if any) that is contained inside two \verb+&+
signs. Use \verb+\nonumber+ on any line to suppress the numbering for that
line. For example:
\se\begin{verbatim}
\begin{eqnarray}
  x&=& 2y^2-(y+1)^2 \nonumber \\
   &=& y^2-2y-1 .
\end{eqnarray}
\end{verbatim}\ee
\noindent typesets as
\se\begin{eqnarray}x&=& 2y^2-(y+1)^2 \nonumber \\
&=& y^2-2y-1 .
\end{eqnarray}\ee\noindent
Observe that the equals signs line up, the first line is unnumbered, and
the final line does not require a \verb+\\+ at the end.

Using the \verb+eqnarray*+ environment allows one to set multi-line equations
with no numbers on any lines:
\se\begin{verbatim}
\begin{eqnarray*}
  x&=& 2y^2-(y+1)^2 \\
   &=& y^2-2y-1 .
\end{eqnarray*}
\end{verbatim}\ee
\noindent typesets as
\se\begin{eqnarray*} x&=& 2y^2-(y+1)^2 \\
 &=& y^2-2y-1 .
\end{eqnarray*}\ee\noindent
Notice that there are no numbers on the lines of the output, even though
the \verb+\nonumber+ command was not used.


To obtain numbers not normally produced by the automatic numbering,
use the \verb+\eqnum{#1}+ command, where \verb+#1+ is the desired
equation number. For example, to get an equation number of
$(3')$,
\se\begin{verbatim}
\begin{equation}
E=mc^2.  \eqnum{3$'$}\label{eq:mynum}
\end{equation}
\end{verbatim}\ee\noindent
{\bf Note:} The \verb+\eqnum+ must come before the \verb+\label+, if any. Also,
numbers assigned by \verb+\eqnum{#1}+ are completely independent of the
automatic numbering; therefore, you must know the number ahead of time and 
{\it must\/} make sure that the number set with \verb+\eqnum+ stays in step
with the automatic numbering.
\verb+\eqnum+ works with both single-line and multi-line equations.
If you wish a series of equations to be a lettered sequence, e.g., (4a),
(4b), and (4c), just include the equation(s) or eqnarray(s) within the {\tt
mathletters} environment.

See \verb+samp0.tex+ to see examples of how all this works.

\subsubsection{Cross-referencing displayed equations}

Although authors will probably not need to cross reference every equation
in text, when a
numbered equation needs to be referred to in text by its number the
\verb+\label{#1}+ and \verb+\ref{#1}+ commands can be used. The
\verb+\label{#1}+ command is used within the equation or the individual
eqnarray line to be referenced, and \verb+\ref{#1}+ may then be used
anywhere in the file to reproduce the correct equation number:
\se\begin{verbatim}
\begin{equation}
  E=mc^2 \label{einstein}
\end{equation}
 It follows from Eq.\ (\ref{einstein}) ...
\end{verbatim} \ee
\noindent typesets as
\se\begin{equation}
E=mc^2 \label{einstein}
\end{equation}
 It follows from Eq.\ (\ref{einstein}) ...\ee
\noindent and
\se\begin{verbatim}
\begin{eqnarray}
  x&=& 2y^2-(y+1)^2 \nonumber\\
   &=& y^2-2y-1 . \label{noteinstein}
\end{eqnarray}
 It follows from Eq.\ (\ref{noteinstein}) ...
\end{verbatim} \ee
\noindent typesets as
\se\begin{eqnarray}
 x&=& 2y^2-(y+1)^2 \nonumber\\
 &=& y^2-2y-1 . \label{noteinstein}
\end{eqnarray}
 It follows from Eq.\ (\ref{noteinstein}) ...\ee


{\bf Note:} incorrect cross referencing will result if \verb+\label{#1}+ is
used in an unnumbered single line equation (i.e., within the \verb+\[+ and
\verb+\]+ commands), or if \verb+\label{#1}+ is used on a line of an
eqnarray that is not being numbered (i.e., a line that has a
\verb+\nonumber+).

\subsection{Figures}

Figures are a part of the paper and should appear as close as possible to
their discussion. They should be input sequentially in the order in which they
are cited in the text, closely following their first citations; \verb+aipproc+
will label and number the captions FIGURE~1, FIGURE~2, etc.


The general setup for a figure to be pasted by hand  on the page will be
\se\begin{verbatim}
\begin{figure} % fig 1
\vspace{2.25in}
\caption{Instrument used for measurements.}
\label{instrumentfig}
\end{figure}
\end{verbatim}\ee
\noindent 
Here, the figure environment contains the following commands:
\begin{itemize}
 \item \verb+\vspace{#1}+ leaves a vertical space equal to \verb+#1+. The
   author must therefore calculate how much space will be needed for each
   figure {\it after being reduced}, and use the  \verb+\vspace{#1}+ command
   to leave space for the insertion of the figure.
 \item \verb+\caption{#1}+ contains the caption for the figure. Fragile
   commands must be immediately preceded by \verb+\protect+.
  See Sec.\ \ref{hints} for a way around this.
 \item \verb+\label{#1}+ works just like it does for equations. This command
   must be placed inside or after---but not before---the \verb+\caption{#1}+
   command. Then the figure number can be reproduced by using the
   \verb+\ref{#1}+ command anywhere in the file.
\end{itemize}

For an Encapsulated Postscript (or EPS) figure to
be included into your paper,
\latexe~users can, input something like this
\se\begin{verbatim}
\begin{figure} % fig 3.2
\centerline{\epsfig{file=fig3-2.ps}}
\caption{Concentration profile.}
\label{F:jfoo:2}
\end{figure}
\end{verbatim}\ee

\subsection{Tables}
\label{sec:tables}
Two environments are provided by \verb+aipproc+ to prepare ``regular tables"
and ``long tables" respectively. The latter is available to \latexe~users only.
 
\subsubsection{Regular tables}
Regular tables are handled as floating inserts
and such table will not break across pages.
 
Each table must begin with \verb+\begin{table}+ and end with
\verb+\end{table}+.  The table commands will set horizontal
lines at the beginning and end of the table; a single horizontal
rule should be set after the column headings by using the
\verb+\tableline+ command. Extra sets of column headings within the table will
require another \verb+\tableline+ to separate the headings from the column
entries. Do not insert any vertical lines in the body of the table.
 
Table cross referencing works just like figure cross referencing: the
\verb+\label{#1}+ command may be used inside of the \verb+\caption{#1}+
command or immediately following it, but not before it. Then 
the  \verb+\ref{#1}+
command is used to cite tables in text.
 
For example,
 
\se\begin{verbatim}
These are shown in Table \ref{table1}.
\begin{table}
\caption{Caption for Table 1 goes here}
\label{table1}
\begin{tabular}{lddd}
\multicolumn{1}{c}{Main sample}& Secondary result&
\multicolumn{1}{c}{Previous result\tablenote{%
   This is tablenote one.}} & Change(\%)}\\
\tableline
Harkonen {\it et al.} & 34.5 & 24.5 & $-$23. \\
Deckard and Batty & 24.5 & 28.5 & $-13$. \\
Marcin & 134.5 & 224.5 & 53. \\
\end{tabular}
\end{table}
\end{verbatim}
\ee
\noindent
\begin{itemize}
 
\item {\em Numerical columns\/} will align on the decimal point (or
decimal points if more than one is is present) if the column specifier,
\verb+`d'+, has been used. This should be used for simple numeric data with a
{\em single\/} decimal point. Material without a decimal point is simply
centered. Notes: entries that start with a decimal point (e.g.,
\verb+.003+) will not be aligned by the decimal point; you should add a
prezero to align the number correctly (e.g., \verb+0.003+). Additionally,
the entry is typeset in separate parts separated by any decimal point(s)
present, so parts of the entry to the left and right of a decimal point
must be able to be typeset separately. For example, \verb+$-1.23$+ will not
work in a \verb+d+ column. You will get a ``missing \$'' error because
\verb+$-1+ is typeset separately from \verb+23$+. Use instead
\verb+$-$1.23+. If multiple decimal points are present then the last is
used for alignment. To escape from the \verb+d+ column use
\verb+\multicolumn+ as usual. See sample files for examples.
 
\item Use \$ delimiters for all math in a table (no displayed equation
commands).
 
\item Use \verb+\tablenote+ command to generate a table footnote. When a
table entry in a \verb+`d'+ column is \verb+\tablenote+'ed, it is always
safe to escape that entry with a \verb+\multicolumn+.
 
\item {\em Extra wide tables\/}
that will not fit into the 5.5-in.\ designation can be turned  broadside
using the \latexe\ package \verb+lscape+.
 
\end{itemize}
 
\subsubsection{Long tables}
\latexe\ users can use \verb+longtable+ package (through the \verb+aiptable+
environment) to prepare tables that span page breaks.
\verb+longtable+ supports automatically continued table heads and feet.
One consequence of this is that these tables will not ``float". It is necessary
to place the tables by hand in the position that should appear. Notes:
\verb+\tablenote+ works the same way, but the \verb+`d'+ column specifier
is not available with \verb+longtable+.
 
Following is an example of a table prepared using the longtable package.
The body of the table, which is the same as in a normal LaTeX tabular
environment, has been omitted.
 
%\se
\begin{verbatim}
\begin{aiptable}
\begin{longtable}{rrrrrr}
\caption{Representative symbiotic stars.}
\label{T:jfoo:2}
\\\aftercapline
Name & Other Name & $\alpha_{2000}$ &
$\delta_{2000}$ & $l_{\rm II}$ & $b_{\rm II}$ \\
\afterheadline\endfirsthead
\caption[]{Continued.} \\
\aftercapline
Name & Other Name & $\alpha_{2000}$ &
$\delta_{2000}$ & $l_{\rm II}$ & $b_{\rm II}$ \\
\afterheadline\endhead
\hline\endfoot
\hline\endlastfoot
\end{verbatim}
\begin{center}
$\cdots$ \par
the body of the table is omitted.\par
$\cdots$ \par
\end{center}
\begin{verbatim}
\end{longtable}
\end{aiptable}
\end{verbatim}
%\ee
\noindent
A complete version of this table with comments explaining its usage is in
\verb+samp2.tex+. See the longtable documentation for detailed
information on the use of the longtable environment.
 
Please note \verb+longtable+ is not avaliable to LaTeX 2.09 users.
 
\subsection{Special characters}

The \verb+aipproc+ style will support all the ``normal'' \TeX{} and  \LaTeX{}
symbols, and in addition, the following symbols from REV\TeX:
\begin{verbatim}
\succsim, \precsim, \gtrsim, \lesssim, 
\corresponds, \lambdabar, \tensor.
\end{verbatim}
The extra symbols and fonts contained in the
AMSFonts collection are available via style options \verb+amsfonts+, 
or \verb+amssymb+ for those \latexe\ users who have the AMSFonts installed.
For \LaTeX\ 2.09 one also needs to have \verb+amssym.def+, and 
\verb+amssym.tex+.

\verb+samp0.tex+ shows what symbols these command produce.

\subsection{References}


The references should be input at the end of each paper using the
\verb+references+ environment. 
\verb+samp0.tex+ shows examples of a variety of reference
entries, e.g., journal, book, etc.

The reference section will be input as a normal \LaTeX\
\verb+references+ environment. Each reference is started with the 
\verb+\bibitem{#1}+ command, where \verb+#1+ 
represents the tag name of the reference. For example, use the
\verb+\cite{#1}+ command anywhere in the text, and the number that is
automatically assigned to the bibitem will be inserted in the output. So,
\se\begin{verbatim}
We have studied the major works \cite{A,B,C}.
     .
     .
     .
\begin{references}
\bibitem{A} Foo, J. S., {\it Journal.} {\bf25},
   1456--1466 (1983).
\bibitem{B} Bar, J. S., {\it Journal.} {\bf15},
   56--66 (1953).
\bibitem{C} Baz, J. S., {\it Journal.} {\bf38},
   156--160 (1993).
\end{references}
\end{verbatim}\ee
\noindent produces
\se
We have studied the major works (1--3).
\begin{verbatim}
    .
    .
    .
\end{verbatim}
% Oh, well, had to fake the references as they were not moving over in the
% example output:
\begin{center}
\bf REFERENCES
\end{center}
\vskip1pc \small \parindent0pt
1.\kern1ex  Foo, J. S., {\it Journal.} {\bf25}, 1456--1466 (1983).\par
2.\kern1ex  Bar, J. S., {\it Journal.} {\bf15}, 56--66 (1953).  \par
3.\kern1ex  Baz, J. S., {\it Journal.} {\bf38}, 156--160 (1993). \par
\ee
You may have noticed that
a cite command that has a list of references will be output with
consecutive reference numbers collapsed; for example, [1,2,7,8,9] will be
output as [1,2,7--9]. No ordering is done before collapsing the list,
so [1,3,2] will be
output as [1,3,2]. If you use a \verb+\cite{#1}+ command with a long list
of tags, your list may take up more than one input line in
the file. Use a \%
character immediately following one of the commas in the list in order to
ensure that you  get the expected results:
\begin{verbatim}
 . . . is shown in \cite{refa,refb,refc,refd,%
refe,reff,refg,refh,refi,refj,refk,refl}
\end{verbatim}
Observe the \% inserted after the comma at the end of
 the first line. This ensures that
the entire list will be processed correctly.

When using the \verb+\label+ and \verb+\bibitem+ commands, try to create 
unique labels so that there will be no collisions of names when all papers
are run together at the end. For example, using \verb+\label{table1}+ is
likely to put your label in conflict with another author's use of the
same label. Here is one scheme that has been used successfully:
Use \verb+\label{X:Y:Z}+, where X is the label type (S = section, T = table,
F = figure, E = equation), Y is the author's initial and lastname, and Z 
is the number
of the thing being labeled. For example, the first table in James Foo's paper
would be labeled with \verb+\label{T:jfoo:1}+, the first figure would be
\verb+\label{F:jfoo:1}+, the second figure would be \verb+\label{F:jfoo:2}+, 
and so on.
Similarly, Smith's 1994 paper is better labeled with
\verb+\bibitem{jfoo:smith94}+ than \verb+\bibitem{smith94}+ since 
\verb+\bibitem{smith}+ is likely to come up more than once.


\subsection{Use other packages with aipproc style}
There are many more packages available for \latexe\ than for \LaTeX\ 2.09. 
One needs to have the right \verb+.sty+ files for his/her version of \LaTeX.

\paragraph{\latexe\ users:} We suggest you get the \verb+graphics+ and 
\verb+tools+ bundles because in most cases they should meet your variety of 
needs. \verb+README+ describes how one may get the files from
ftp.shsu.edu and install them.

\paragraph{\LaTeX\ 2.09 users:} You can obtain the required \verb+sty+ files 
          via anonymous ftp from ftp.shsu.edu in the following directories:

\noindent  tex-archive/macros/latex209/contrib/epsfig
           tex-archive/macros/latex209/contrib/geom
           tex-archive/macros/latex209/contrib/misc
           tex-archive/macros/latex209/distribs/latex/sty.
 
\subsection{Troubleshooting}
\label{hints}
 
 
{\bf Question: How do I get lowercase letters in the \verb+\section{#1}+
command?} All text in the \verb+\section{#1}+,
\verb+\righthead{#1}+, and \verb+\lefthead{#1}+ commands is automatically set
uppercase. If a lowercase letter is needed, just use \verb+\lowercase{x}+.
This also works in math mode.
 
{\bf Problem: I am getting error messages on the lines of my
\verb+\section{#1}+,
\verb+\subsection{#1}+,
\verb+\subsubsection{#1}+, or
\verb+\caption{#1}+ commands, and I can't understand why!}
You may have a so-called ``fragile'' command in a section heading or
caption. \LaTeX's solution to the problem is to immediately precede the
fragile command with \verb+\protect+.
 
An easier solution is available: just pass empty optional arguments to the
commands. For example, instead of using
\se\begin{verbatim}
\caption{#1}
\end{verbatim}\ee
just use
\se\begin{verbatim}
\caption[]{#1}
\end{verbatim}\ee
You do not need to worry about how this works unless you are interested in
producing a table of contents, list of figures, or list of tables. Otherwise
you may safely use this technique for all section headings and captions.
 
{\bf Question: \TeX\ runs out of space while typesetting my captions. What
do I do?} Use the empty optional arguments outlined in the answer to the
preceding question. The length of your caption is overflowing TeX's buffer.
Using the empty  optional argument fixes this problem.
 
%\newpage
 
{\bf Question: \LaTeX{} is not putting my figure(s)/table(s) where I
think they should go. How can I change this?} \LaTeX{} will normally do an
acceptable job of placing floating elements on the page. If you have problems,
see the discussion in
Appendix C.8.1 of the \LaTeX{} manual.
Please note that tables and figures should be set at either the top or bottom
of the page whenever possible, not in the middle.
 



\subsection{Support}

For technical support and information, contact:

\leftskip2pc\parindent0pt

\vskip8pt
Cahrles Doering \\Conference Proceedings\\
American Institute of Physics\\
Woodbury, NY 11797\\[4pt]
Phone: (516) 576-2475\\
e-mail: cdoering@aip.org
 


\end{document}


% End of file aipproc.tex. Documentation file for AIP Press aipproc style.
